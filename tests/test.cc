/**
 * @file      test.cc
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#include "options/options.hh"

// Test of the above
int main(int argc, char** argv)
{
  using namespace options;

  command_line&   cl = command_line::instance(argv[0]);
  separator       s1("Normal options");
  bool_option     h('h',      {"help","usage"}, "Help", false);
  bool_option     b('b',      "bool",                    "Boolean",      false);
  int_option      i('i',      {"int","var"},    "NUMBER","Integer",      0);
  float_option    f({'f','x'},"float",          "NUMBER","Float",        0.f); 
  string_option   s({'s','n'},{"string","name"},"STRING","String",       "");
  separator       s2("Stream options");
  istream_option  j('j',      "istream",        "FILE", "Input stream");
  ostream_option  o('o',      "ostream",        "FILE", "Output stream");
#if __cplusplus >= 201703L
  separator       s3("Path options");
  path_option     p('p',      "path",           "PATH", "Path", "");
#endif
  separator       s4("Mapping options");
  map_option<int> m('m',      "map",            "KEY",   "Key to value",
		    {{"a",0},{"q",42},{"*",0}},"a");
  separator       s5("Multi-valued options");
  bools_option    B('B',      "bools",                   "Booleans");
  ints_option     I('I',      "ints",          "NUMBERS","Integers",{1,2,3});
  floats_option   F('F',      "floats",        "NUMBERS","Floats",  {.1,.2}); 
  strings_option  S('S',      "strings",       "STRINGS","Strings", {"a","b"});

  if (!cl.parse(argc,argv)) return 1;
  if (h) {
    cl.usage(std::cout);
    return 0;
  }
  cl.show(std::cout);

#if __cplusplus >= 201703L
  std::cout << p
	    << " parent=" << p->parent_path()
	    << " filename=" << p->filename()
	    << std::endl;
#endif

  for (auto ff : F) std::cout << ff << std::endl;
  return 0;
}
// Local Variables:
//   mode: C++ 
// End:
