# A simple command line options class library 

This package provide some simple command line option classes that 
are easy and intuitive to use.  

## Quick start 

In your main program, declare the handler of command line options 

```c++
int main(int argc, char** argv)
{
  auto& cl = options::command_line::instance(argv[0]);
```

This is a _singleton_ object meaning we have only one of these (hence,
the reference specification is _crucial_).  If you worry about
_thread-safety_, don't.  A program can only have _one_ entry point,
and the `options::command_line` object is only ever accessed in that
entry point (directly or indirectly).

Next, add options by declaring these 

```c++
  options::bool_option help('h',"help","Show this help",false);
  options::int_option  num ('i',"int","NUMBER","An integer", 42);
```

In general, options constructors takes 5 arguments 

- Short-form option.  A single character `char` or a sequence of
  such.  Each character will produce a command-line option of the form
  `-c` where `c` is the character 
- Long-form option.  A single string or a sequence of strings
  (`std::string`).  Each string will produce the command-line option
  `--string` where `string` is the string. 
- A place-holder name.  This specifies what the help text dummy value
  should be shown as - e.g., `NUMBER` above 
- A help string.  An explanatory description of the option being
  defined.  This can be as long as you like - the text will be wrapped
  appropriately. 
- A default value.  The default value of the option if not specified
  by the user on the command line. 
  
Note, boolean options (or _flags_) of class `options::bool_option` or
`bools_option` does not accept a place-holder name. 

Once all your options have been defined, ask the command line handler
to process the command line 

```c++
  if (not cl.parse(argc,argv)) return 1;
```

Here, if the parsing is not successful, we return from the program
with the failure code `1`.  If the user has asked to see which command
line options are available (e.g., using the option `-h` or `--help`),
we can do that by 

```c++
  if (help) {
    cl.usage(std::cout);
    return 0;
  }
```

This will print an overview of the available option to the passed
output stream and end the program with the success code `0`.  Note, we
can pass _any_ output stream.

After this point in our program we want to use the values of the
options in a program flow.  For example 

```c++
  for (int i = 0; i < num; i++) std::cout << i << std::endl;
```

Owing to the definition of `operator T()` in the option classes of
type `T`, we can simply use the option object as if it was a variable
of type `T`.  If we want explicit access to the value or the value is
passed in a context that does not do type implicit conversions, we can
access the value through the `.value` member function, or call
`T::f` member functions via the `operator->()` member function of the
options. 

## Predefined option types 

### Single valued options 

The library defines the following options

- `options::bool_option`
- `options::int_option`
- `options::float_option`
- `options::string_option` These are simple options that correspond to
  the types `bool`, `int`, `float` and `std::string`.
- `options::ostream_option` 
- `options::istream_option` These are options that correspond to
  streams.  When given, the library will open the corresponding file
  (for write _or_ read, respectively) and the option will have the
  value of the corresponding stream.  Note, the special value `-`
  corresponds to `std::cout` and `std::cin`, respectively. 
- `options::path_option` (requires `std::filesystem` namespace of
  C++17).  Options of this kind will be file system paths.

All of these options only take a single value.  That is, if the user
gave an option more than once, then only the last value will be used.

### Multi-valued options 

The library does, however, define option kinds that can be given
multiple times are multi-valued.  These are 

- `options::ints_option` 
- `options::floats_option` 
- `options::strings_option` 
  Similar to the single value options above. 
  
One can iterate (including via a range) over these kinds of options. 

### Mapping options 

Another kind of option allows for preset value, optionally with an
free value.  

- `options::map_option<T>` 
  An option that maps arguments to preset value of type `T` 
  
Similarly we have a class for a mapping option that can take on
multiple values 

- `options::multi_map_option<T>` 

As the use of this is a little particular, let see an example 

```c++
  options::map_option<int> fruit('f',"fruit","FRUIT","A fruit",
                                 {{"apple", 2},
                                  {"banana", 4},
                                  {"plum", 1}}, "apple");
```                     

This define the option `-f` or `--fruit` which can _only_ take the
arguments `apple`, `banana`, or `plum`.  When used as a value, this
option will be of `int` type and take on the values `2`, `4`, or `1`,
respectively.  The default choice is `apple`. 

This kind of option is useful if one wants to restrict the possible
choices given to an option. 

The special key `*` allows for _any_ value of type `T` to be given as
argument.  For example, suppose we want to give the user the choice of
random number seed as follows 

- A fixed seed - e.g., for testing 
- A seed retrieved from `std::random_device` 
- The current time (seconds since epoch)
- Any positive number the user may choose 

then we can do 

```c++
  options::map_option<int> seed('s',"seed","NUMBER","Random number seed",
                                {{"default",123456},
                                 {"random",-1},
                                 {"time",0},
                                 {"*",-2}}, "default")
```

The user can now do one of 

```sh
--seed default 
--seed random 
--seed time 
--seed N
```

where `N` is any number. 

Once the command line options has been parsed, we can do 

```c++
  int iseed = seed;
  if (iseed == -1) iseed = std::random_device()();
  if (iseed == 0)  iseed = std::time(NULL);
    
  std::default_random_engine eng(iseed);
```

Often, it is convenient to make a custom option class that derives
from `options::map_option<T>`.  For example, we could make a
`seed_option` class that takes care of all of the above 

```c++
struct seed_option : public map_option<int>
{
  using base=map_option<int>
  seed_option() : base('s',"seed","NUMBER","Random number seed",
                       {{"default",123456},
                        {"random",-1},
                          {"time",0},
                          {"*",-2}}, "default")
  {}
  bool parse(const std::string& arg="") override 
  {
    if (!base::parse(arg)) return false;
    switch (_value) {
    case -1: {
      std::random_device rd;
      _value = rd();
    }
      break;
    case 0:
      _value = std::time(NULL);
      break;
    }
    return true;
  }
};
```

Now we can simply use the `seed` option as a value. 

## Adding more option types 

Most of the predefined options uses the class template
`options::option<T>` - either by direct specialisation or instantation or
as base class.  One can use the same strategy to add other option
types.  

### Instantation 

The class template `options::option<T>` uses the service
`options::option_trait<T>` to work out how to convert strings to and
from values of `T`.  The default implementation uses `std::istream`
get-from and `std::ostream` put-to conversion of type `T` - i.e.,

- For value `v` to string `s`, uses `s << v` 
- For string `s` to value `v`, uses `s >> v` 

If this is not appropriate for the user type `T`, then one can make a
specialisation of `options::option_trait<T>`.  For example, the
`path_option` is the instantation

```c++
using path_option=option<std::filesystem::path> 
```

which uses the specialisation 

```c++
template <>
struct option_trait<std::filesystem::path>
{
  static void from_string(std::filesystem::path& p, const std::string& s)
  {
    p = s;
  }
  static std::string to_string(const std::filesystem::path& p)
  {
    return p.string();
  }
};
```

### Derivation 

If a specialisation of `options::option_trait` is not enough, one can
derive a class from `options::option<T>`.  For example, the
`options::map_option<T>` class template derives from
`options::option<T>` to do additional computations.  Similarly,
options `options::istream` and `options::ostream` derive from
`options::base_option` but has extra logic to open (and close)
underlying files and return the appropriate stream object.

## Usage output 

The library does not define any standard help option, but as outlined
above, it is quite simple to define one. 

When invoking `options::command_line::usage`, the options will be
listed in the order defined by the program.  If one wants to define
sections of options, one can add define `options::separator` objects
in the flow of option definitions.  This class takes a single string
argument which will be the title of that section.  For example 

```c++
int main(int argc, char** argv)
{
  auto cl = options::command_line::instance(argv[0]);
  options::separator       ("General options");
  options::bool_option help('h',"help","Show this help",false);
  options::separator       ("Specific options");
  options::int_option  num ('i',"int","NUMBER","An integer", 42);

  if (not cl.parse(argc,argv)) return 1;

  if (help) {
    cl.usage(std::cout);
      return 0;
  }
   
  return 0;
}
```

This will cause the section headers `General options` and `Specific
options` to be printed before the `help` and `int`options,
respectively.  The text of the the header can be as long as you like
and will automatically be wrapped on output. 

