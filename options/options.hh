/**
 * @file      options.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_options_hh
#define options_options_hh
#include "options/separator.hh"
#include "options/bool_option.hh"
#include "options/map_option.hh"
#include "options/command_line.hh"
#include "options/stream_option.hh"

namespace options
{
  //==================================================================
  using int_option     = option<int>;
  using float_option   = option<float>;
  using string_option  = option<std::string>;
  using ostream_option = option<std::ostream>;
  using istream_option = option<std::istream>;
#if __cplusplus >= 201703L
  using path_option    = option<std::filesystem::path>;
#endif
  
  using ints_option     = multi_option<int>;
  using floats_option   = multi_option<float>;
  using strings_option  = multi_option<std::string>;
}
#endif
// Local Variables:
//   mode: C++ 
// End:
