/**
 * @file      bool_option.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_bool_option_hh
#define options_bool_option_hh
#include "options/multi_option.hh"

namespace options
{
  //------------------------------------------------------------------
  /** 
   * Boolean specialisation 
   */
  struct bool_option : public option<bool>
  {
    using base=option<bool>;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    /** 
     * Defines a flag 
     *
     * @param shrt Short option 
     * @param lng  Long option 
     * @param desc Description 
     * @param def  Default value
     */
    bool_option(const char_initializer_list&   shrt,
		const string_initializer_list& lng,
		const std::string&             desc,
		bool                           def=false)
      : base(shrt,lng,"",desc,def)
    {}
    /** 
     * Defines a flag 
     *
     * @param shrt Short option 
     * @param lng  Long option 
     * @param desc Description 
     * @param def  Default value 
     */
    bool_option(char               shrt,
		const std::string& lng,
		const std::string& desc,
		bool               def=false)
      : bool_option({shrt},{lng},desc,def)
    {}
    /** 
     * Defines a flag 
     *
     * @param shrt Short option 
     * @param lng  Long option 
     * @param desc Description 
     * @param def  Default value
     */
    bool_option(char                           shrt,
		const string_initializer_list& lng,
		const std::string&             desc,
		bool                           def=false)
      : bool_option({shrt},lng,desc,def)
    {}
    /** 
     * Defines a flag 
     *
     * @param shrt Short option 
     * @param lng  Long option 
     * @param desc Description 
     * @param def  Default value
     */
    bool_option(const char_initializer_list& shrt,
		const std::string&           lng,
		const std::string&           desc,
		bool                         def=false)
      : bool_option(shrt,{lng},desc,def)
    {}
  };
  struct bools_option : public multi_option<bool>
  {
    using base=multi_option<bool>;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    using values_type=typename base::values_type;
    /** 
     * Defines a flag 
     *
     * @param shrt Short option 
     * @param lng  Long option 
     * @param desc Description 
     * @param def  Default value
     */
    bools_option(char               shrt,
		 const std::string& lng,
		 const std::string& desc,
		 const values_type& def={})
      : base(shrt,lng,"",desc,def)
    {}
  };
}
#endif
