/**
 * @file      options/stream_option.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_stream_option_hh
#define options_stream_option_hh
#include "options/base.hh"
#include <fstream>
#include <filesystem>

namespace options
{
  //------------------------------------------------------------------
  /** 
   * Output stream specialisation 
   */
  template <>
  struct option<std::ostream> : public base_option
  {
    using base=base_option;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(const char_initializer_list&   shrt,
	   const string_initializer_list& lng,
	   const std::string&             arg,
	   const std::string&             desc,
	   std::ostream&                  def=std::cout)
      : base_option(shrt,lng,arg,desc), _ptr(&def), _obj(0), _nam("-")
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(char               shrt,
	   const std::string& lng,
	   const std::string& arg,
	   const std::string& desc,
	   std::ostream&      def=std::cout)
      : option({shrt},{lng},arg,desc,def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(char                           shrt,
	   const string_initializer_list& lng,
	   const std::string&             arg,
	   const std::string&             desc,
	   std::ostream&                  def=std::cout)
      : option({shrt},lng,arg,desc,def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(const char_initializer_list& shrt,
	   const std::string&           lng,
	   const std::string&           arg,
	   const std::string&           desc,
	   std::ostream&                def=std::cout)
      : option(shrt,{lng},arg,desc,def)
    {}
    ~option()
    {
      close();
    }
    /** 
     * Parse an argument 
     *
     * @param arg Argument 
     *
     * @return true on success 
     */
    bool parse(const std::string& arg="")
    {
      close();

      _nam = arg;
      if (arg == "-") {
	_ptr = &std::cout;
	return true;
      }
      _obj = new std::ofstream(arg.c_str());
      _ptr = _obj;

      return true;
    }
    /** @return Value as given type */
    operator std::ostream&() const { return *_ptr; }
    /** @return Value as given type */
    std::ostream& value() const { return *_ptr; }
    /** @return String representation of value */
    std::string to_string() const { return _nam; }
  protected:
    void close()
    {
      if (_obj) delete _obj;
      _obj = 0;
    }
    /** The value */
    std::ostream*  _ptr;
    std::ofstream* _obj;
    std::string    _nam;
  };

  //------------------------------------------------------------------
  /** 
   * Output stream specialisation 
   */
  template <>
  struct option<std::istream> : public base_option
  {
    using base=base_option;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(const char_initializer_list&   shrt,
	   const string_initializer_list& lng,
	   const std::string&             arg,
	   const std::string&             desc,
	   std::istream&                  def=std::cin)
      : base_option(shrt,lng,arg,desc), _ptr(&def), _obj(0), _nam("-")
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(char               shrt,
	   const std::string& lng,
	   const std::string& arg,
	   const std::string& desc,
	   std::istream&      def=std::cin)
      : option({shrt},{lng},arg,desc, def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(char                           shrt,
	   const string_initializer_list& lng,
	   const std::string&             arg,
	   const std::string&             desc,
	   std::istream&                  def=std::cin)
      : option({shrt},lng,arg,desc,def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(const char_initializer_list& shrt,
	   const std::string&           lng,
	   const std::string&           arg,
	   const std::string&           desc,
	   std::istream&                def=std::cin)
      : option(shrt,{lng},arg,desc,def)
    {}
    ~option()
    {
      close();
    }
    /** 
     * Parse an argument 
     *
     * @param arg Argument 
     *
     * @return true on success 
     */
    bool parse(const std::string& arg="")
    {
      close();

      _nam = "-";
      if (arg == "-") {
	_ptr = &std::cin;
	return true;
      }
      _obj = new std::ifstream(arg.c_str());
      _ptr = _obj;

      return true;
    }
    /** @return Value as given type */
    operator std::istream&() const { return *_ptr; }
    /** @return Value as given type */
    std::istream& value() const { return *_ptr; }
    /** @return String representation of value */
    std::string to_string() const { return _nam; }
  protected:
    void close()
    {
      if (_obj) delete _obj;
      _obj = 0;
    }
    /** The value */
    std::istream*  _ptr;
    std::ifstream* _obj;
    std::string    _nam;
  };
}
#endif
