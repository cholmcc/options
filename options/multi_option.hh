/**
 * @file      multi_option.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_multi_option_hh
#define options_multi_option_hh
#include "options/option.hh"
#include <list>


namespace options
{
  //------------------------------------------------------------------
  template <typename T, typename Trait=option_trait<T>>
  struct multi_option : public base_option
  {
    using trait_type=Trait;
    using values_type=std::list<T>;
    using iterator=typename values_type::iterator;
    using const_iterator=typename values_type::const_iterator;
    using base=base_option;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    multi_option(const char_initializer_list&   shrt,
		 const string_initializer_list& lng,
		 const std::string&             arg,
		 const std::string&             desc,
		 const values_type&             def={})
      : base_option(shrt,lng,arg+"(*)",desc), _values(def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    multi_option(char               shrt,
		 const std::string& lng,
		 const std::string& arg,
		 const std::string& desc,
		 const values_type& def={})
      : multi_option({shrt},{lng},arg,desc,def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    multi_option(char                           shrt,
		 const string_initializer_list& lng,
		 const std::string&             arg,
		 const std::string&             desc,
		 const values_type&             def={})
      : multi_option({shrt},lng,arg,desc,def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    multi_option(const char_initializer_list& shrt,
		 const std::string&           lng,
		 const std::string&           arg,
		 const std::string&           desc,
		 const values_type&           def={})
      : multi_option(shrt,{lng},arg,desc,def)
    {}
    /** 
     * Parse an argument 
     *
     * @param arg Argument 
     *
     * @return true on success 
     */
    virtual bool parse(const std::string& arg="")
    {
      T v;
      trait_type::from_string(v,arg);
      _values.push_back(v);

      return true;
    }
    /** @return iterator pointing to beginning */
    iterator begin() { return _values.begin(); }
    /** @return iterator pointing to beginning */
    const_iterator begin() const { return _values.begin(); }
    /** @return iterator pointing to ending */
    iterator end() { return _values.end(); }
    /** @return iterator pointing to ending */
    const_iterator end() const { return _values.end(); }
    
    /** @return Value as given type */
    operator values_type() const { return _values; }
    /** @return Value as given type */
    const values_type& values() const { return _values; }
    /** Access */
    const values_type* operator->() const { return &_values; }
    /** @return String representation of value */
    std::string to_string() const
    {
      std::string s;
      int         comma=0;
      for (auto v : _values)
	s += (comma++ ? "," : "") + trait_type::to_string(v);
      return s;
    }
  protected:
    /** The value */
    values_type _values;
  };
}
#endif
