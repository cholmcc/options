/**
 * @file      options/command_line.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_command_line_hh
#define options_command_line_hh
#include "options/base.hh"
#include "options/bad_value.hh"
#include <list>
#include <map>

namespace options
{
  //==================================================================
  /** 
   * Manager of options (singleton)
   */
  struct command_line
  {
    using arguments_type=std::list<std::string>;
    /**
     * @param name Name to give singleton if to be created 
     * @param more More help to print after options 
     *
     * @return Singleton manager 
     */
    static command_line& instance(const std::string& name="",
				  const std::string& more="")
    {
      if (!_instance) _instance = new command_line(name,more);

      return *_instance;
    }
    /** 
     * @param argc Number of arguments 
     * @param argv Arguments 
     *
     * @return true on success 
     */
    bool parse(size_t argc, char** argv)
    {
      try {
	for (size_t i = 1; i < argc; ++i) {
	  auto* opt = find(argv[i]);
	  if (!opt) {
	    _args.push_back(argv[i]);
	    continue;
	  }
	  
	  bool  ok  = false;
	  if (opt->has_argument()) {
	    if (argv[i][1] != '-' && std::string(argv[i]).size() > 2)
	      ok = opt->parse(&argv[i][2]);
	    else if (i == argc-1)
	      throw std::runtime_error("Missing argument for "+opt->lng());
	    else
	      ok = opt->parse(argv[++i]);
	  }
	  else
	    ok = opt->parse();

	  if (!ok)
	    throw bad_value(opt->lng(),argv[i]);
	}
      } catch(std::exception& e) {
	std::cerr << e.what() << std::endl;
	return false;
      }
      return true;
    }
    /** 
     * Add an option 
     *
     * @param opt Option to add 
     * @param shrts Short option aliases 
     * @param lngs  Long option aliases 
     */
    void add(base_option& opt,
	     const std::vector<char>&        shrts,
	     const std::vector<std::string>& lngs)
    {
      char               shrt = opt.shrt();
      const std::string& lng  = opt.lng();
      if ((shrt and shrt != ' ') or not lng.empty()) {
	if (not lng.empty() and find_it(lng,_long_map))
	  throw std::runtime_error("Option --"+lng+" already exists");
	if (shrt and shrt != ' ' and find_it(shrt,_short_map))
	  throw std::runtime_error("Option -"+std::string(1,shrt)
				   +" already exists");

	if (not lng.empty())      _long_map[lng]   = &opt;
	if (shrt and shrt != ' ') _short_map[shrt] = &opt;

	for (auto s : shrts) _short_map[s] = &opt;
	for (auto l : lngs)  _long_map[l]  = &opt;
      }
      _options.push_back(&opt);
    }
    /** 
     * Show usage 
     *
     * @param o output stream 
     * @param wrap Wrapping column 
     */
    void usage(std::ostream& o, size_t wrap=75) const
    {
      o << "Usage: " << _name << " [OPTIONS] " << _more << "\n\n"
	<< "Options:\n";
      size_t mlng = 0;
      size_t marg = 0;
      size_t mval = 0;
      for (auto r : _options) {
	mlng = std::max(r->lng()      .size(), mlng);
	marg = std::max(r->argument() .size(), marg);
	mval = std::max(r->to_string().size(), mval);
      }
      for (auto r : _options) r->usage(o, mlng,marg,mval,wrap);
    }
    /** 
     * Show the option values 
     *
     * @param o Output stream 
     */
    void show(std::ostream& o)
    {
      size_t mtit = 0;
      for (auto q : _options) {
	std::string t = q->title();
	mtit           = std::max(t.size(), mtit);
      }
      for (auto q : _options) q->show(o,mtit);
      o << "=== Arguments ===\n";
      for (auto a : _args)
	o << "\t" << a << std::endl;
    }
    /** 
     * Get the remaining non-option arguments 
     */
    const arguments_type& arguments() const { return _args; }
  protected:
    /** Singleton */
    static command_line* _instance;
    /** Name - typically program name */
    std::string _name;
    /** Argument help */
    std::string _more;
    /** Constructor */
    command_line(const std::string& name, const std::string& more)
      : _name(name),
	_more(more)
    {}
    template <typename Arg>
    base_option* find_it(const Arg& arg,
			 const std::map<Arg,base_option*>& cont) const
    {
      auto it = cont.find(arg);
      if (it != cont.end()) return it->second;
      return 0;
    }
    /** Find an option
     *
     * @param arg Option to look for 
     *
     * @return Pointer to option, or null if not an option
     */
    base_option* find(const std::string& arg) const
    {
      if (arg[0] == '-') {
	if (arg[1] != '-') 
	  return find_it(arg[1],_short_map);
	return find_it(arg.substr(2),_long_map);
      }
      return 0;
    }
    using short_map=std::map<char,base_option*>;
    using long_map=std::map<std::string,base_option*>;
    
    /** Map from short to option */
    short_map _short_map;
    /** Map from long to short */
    long_map _long_map;
    /** Remaining arguments */
    std::list<base_option*> _options;
    /** Remaining arguments */
    std::list<std::string> _args;
  };
  //------------------------------------------------------------------
  /* Singleton */
  command_line* command_line::_instance = 0;

  //------------------------------------------------------------------
  void base_option::_register()
  {
    command_line& cl = command_line::instance();
    cl.add(*this,_short_alias,_long_alias);
  }
}
#endif
