/**
 * @file      map_option.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_map_option_hh
#define options_map_option_hh
#include "options/multi_option.hh"
#include "options/utils.hh"
#include "options/bad_value.hh"
#include <map>

namespace options
{
  //==================================================================
  template <typename T>
  struct map_option : option<T>
  {
    using base=option<T>;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    using map_type=std::map<std::string,T>;
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    map_option(const char_initializer_list&   shrt,
	       const string_initializer_list& lng,
	       const std::string&             arg,
	       const std::string&             desc,
	       const map_type&                values,
	       const std::string&             def)
      : option<T>(shrt,lng,arg,desc,T()),
	_values(values),
	_key(def)
    {
      this->_value = get(def);
      this->_desc += " - one of";
      for (auto i = _values.begin(); i != _values.end(); ++i) 
	this->_desc += "\n- " + (i->first == "*" ? arg : i->first);
    }
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    map_option(const char&        shrt,
	       const std::string& lng,
	       const std::string& arg,
	       const std::string& desc,
	       const map_type&    values,
	       const std::string& def)
      : map_option({shrt},{lng},arg,desc,values,def)
    {}
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    map_option(char&                          shrt,
	       const string_initializer_list& lng,
	       const std::string&             arg,
	       const std::string&             desc,
	       const map_type&                values,
	       const std::string&             def)
      : map_option({shrt},lng,arg,desc,values,def)
    {}
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    map_option(const char_initializer_list& shrt,
	       const std::string&           lng,
	       const std::string&           arg,
	       const std::string&           desc,
	       const map_type&              values,
	       const std::string&           def)
      : map_option(shrt,{lng},arg,desc,values,def)
    {}
    /** 
     * Parse an argument 
     *
     * @param arg Argument 
     *
     * @return true on success 
     */
    virtual bool parse(const std::string& arg="")
    {
      _key         = arg;
      this->_value = get(arg);
      return true;
    }
    std::string to_string() const { return _key; }
    operator T() const { return this->_value; }
  protected:
    T get(const std::string& key) const
    {
      auto it = _values.find(key);
      if (it == _values.end()) {
	it = _values.find("*");
	if (it == _values.end())
	  throw bad_value(this->lng(), key);

	std::stringstream s(key);
	T val;
	s >> val;
	return val;
      }
      return it->second;
    }
    map_type _values;
    std::string _key;
  };
  //==================================================================
  /** 
   * A mapping of option values to real values, with multiple
   * possible values
   */
  template <typename T>
  struct multi_map_option : public multi_option<T>
  {
    using base=multi_option<T>;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    using values_type=typename base::values_type;
    using iterator=typename values_type::iterator;
    using const_iterator=typename values_type::const_iterator;
    using map_type=std::map<std::string,values_type>;
    using keys_type=std::list<std::string>;
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    multi_map_option(const char_initializer_list&   shrt,
		     const string_initializer_list& lng,
		     const std::string&             arg,
		     const std::string&             desc,
		     const map_type&                values,
		     const string_initializer_list& def)
      : base(shrt,lng,arg,desc,{}),
	_choices(values),
	_keys(def)
    {
      for (auto k : def) this->parse(k);
      this->_desc += " - one of";
      for (auto i = _choices.begin(); i != _choices.end(); ++i) 
	this->_desc += "\n- " + (i->first == "*" ? arg : i->first);
    }
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    multi_map_option(const char&                    shrt,
		     const std::string&             lng,
		     const std::string&             arg,
		     const std::string&             desc,
		     const map_type&                values,
		     const string_initializer_list& def)
      : multi_map_option({shrt},{lng},arg,desc,values,def)
    {}
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    multi_map_option(char&                          shrt,
	       const string_initializer_list& lng,
	       const std::string&             arg,
	       const std::string&             desc,
	       const map_type&                values,
	       const string_initializer_list& def)
      : multi_map_option({shrt},lng,arg,desc,values,def)
    {}
    /** 
     * @param shrt   Short option 
     * @param lng    Long option 
     * @param arg    Argument dummy 
     * @param desc   Description 
     * @param values Mapping from string to value
     * @param def    Default value
     */
    multi_map_option(const char_initializer_list&   shrt,
	       const std::string&             lng,
	       const std::string&             arg,
	       const std::string&             desc,
	       const map_type&                values,
	       const string_initializer_list& def)
      : multi_map_option(shrt,{lng},arg,desc,values,def)
    {}
    /** 
     * Parse an argument 
     *
     * @param arg Argument 
     *
     * @return true on success 
     */
    virtual bool parse(const std::string& arg="")
    {
      _keys.push_back(arg);
      this->_values.splice(this->_values.end(),get(arg));
      return true;
    }
  protected:
    values_type get(const std::string& key) const
    {
      auto it = _choices.find(key);
      if (it == _choices.end()) {
	it = _choices.find("*");
	if (it == _choices.end())
	  throw bad_value(this->lng(), key);

	// Would like to do the thing commented below, by C++11
	// doesn't like it.
	std::list<std::string> parts;
	options::utils::csplit(parts,key,",");
	// auto parts = options::utils::split<>(key,",");
	
	values_type v;
	for (auto p : parts) {
	  std::stringstream s(p);
	  T val;
	  s >> val;
	  v.push_back(val);
	}
	return v;
      }
      return it->second;
    }
    map_type _choices;
    keys_type _keys;
  };
}
#endif
