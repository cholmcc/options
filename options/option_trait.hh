/**
 * @file      option_trait.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_trait_hh
#define options_trait_hh
#include <sstream>
#if __cplusplus >= 201703L
#include <filesystem>
#endif

namespace options
{

  //==================================================================
  template <typename T>
  struct option_trait
  {
    static void from_string(T& v, const std::string& s)
    {
      std::stringstream str(s);
      str >> v;
    }
    static std::string to_string(const T& v)
    {
      return std::to_string(v);
    }
  };
  //------------------------------------------------------------------
  template <>
  struct option_trait<std::string>
  {
    static void from_string(std::string& v, const std::string& s)
    {
      v = s;
    }
    static std::string to_string(const std::string& s) { return s; }
  };
  //------------------------------------------------------------------
  template <>
  struct option_trait<bool>
  {
    static void from_string(bool& v, const std::string&) { v = !v; }
    static std::string to_string(const bool& v) { return std::to_string(v); }
  };
#if __cplusplus >= 201703L
  //------------------------------------------------------------------
  template <>
  struct option_trait<std::filesystem::path>
  {
    static void from_string(std::filesystem::path& p, const std::string& s)
    {
      p = s;
    }
    static std::string to_string(const std::filesystem::path& p)
    {
      return p.string();
    }
  };
#endif 
}
#endif
