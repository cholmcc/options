/**
 * @file      bad_value.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_bad_value_hh
#define options_bad_value_hh
#include <stdexcept>

namespace options
{
  //------------------------------------------------------------------
  /** 
   * Exception thrown on bad argument value
   */
  struct bad_value : public std::runtime_error
  {
    bad_value(char shrt, const std::string& arg)
      : std::runtime_error("Bad value \""+arg+"\" for option -"
			   +std::to_string(shrt))
    {}
    bad_value(const std::string& lng, const std::string& arg)
      : std::runtime_error("Bad value \""+arg+"\" for option -"+lng)
    {}
  };
}
#endif
