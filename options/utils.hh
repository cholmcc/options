/**
 * @file      utils.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_utils_hh
#define options_utils_hh
#include <string>
#include <sstream>
#include <list>

namespace options
{
  namespace utils
  {
    //------------------------------------------------------------------
    /** 
     * Wrap text to column with possible bound indent 
     */
    std::string wrap(const std::string& txt, size_t last, size_t ind=0)
    {
      // Make copy of input with as many spaces in front as the requested
      // indent.
      std::string i = std::string(ind,' ');
      std::string t = i+txt;
    
      size_t s = last;
      size_t o = 0;
      while (s < t.size() and
	     (s = t.find_last_of(" \t\n",s)) != std::string::npos and
	     s > o){
	t.replace(s,1,"\n"+i);
	o =  s+ind+1;
	s += last+1;
      }
      return t;
    }
    //--------------------------------------------------------------------
    template <typename Container>
    std::string join(const Container& c, const std::string& sep=",")
    {
      std::stringstream s;
      int comma = 0;
      for (auto e : c) s << (comma++ ? sep : std::string()) << e;
      return s.str();
    }
    //--------------------------------------------------------------------
    template <typename Container, typename Formatter>
    std::string join(const Container& c,
		     const std::string& sep,
		     Formatter format)
    {
      std::stringstream s;
      int comma = 0;
      for (auto e : c) s << (comma++ ? sep : std::string()) << format(e);
      return s.str();
    }
    //--------------------------------------------------------------------
    // `class` below should really be `typename` but not allowed in C++11
    template <template <typename> class Container=std::list>
    Container<std::string> split(const std::string& str,
				 const std::string& del,
				 bool keepEmpty=false)
    {
      Container<std::string> r;
      size_t s = 0;
      size_t p = 0;
      while ((p = str.find(del, s)) != std::string::npos) {
	if (s == p and not keepEmpty) continue;

	r.emplace_back(str.substr(s,p));
	s = p + del.length();
      }
      if (s != str.size()-1) r.emplace_back(str.substr(s));
      return r;
    }
    //--------------------------------------------------------------------
    // `class` below should really be `typename` but not allowed in C++11
    template <typename Container=std::list<std::string>>
    Container& csplit(Container&         r,
		      const std::string& str,
		      const std::string& del,
		      bool keepEmpty=false)
    {
      size_t s = 0;
      size_t p = 0;
      while ((p = str.find(del, s)) != std::string::npos) {
	if (s == p and not keepEmpty) continue;

	r.emplace_back(str.substr(s,p));
	s = p + del.length();
      }
      if (s != str.size()-1) r.emplace_back(str.substr(s));
      return r;
    }
  }
}
#endif
