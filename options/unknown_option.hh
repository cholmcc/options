/**
 * @file      unknown_option.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_unknown_option_hh
#define options_unknown_option_hh
#include <stdexcept>

namespace options
{
  //==================================================================
  /** 
   * Exception thrown on unknown option
   */
  struct unknown_option : public std::runtime_error
  {
    unknown_option(char shrt)
      : std::runtime_error("Unknown option: "+std::to_string(shrt))
    {}
    unknown_option(const std::string& lng)
      : std::runtime_error("Unknown option: "+lng)
    {}
  };
}
#endif
