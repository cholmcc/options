/**
 * @file      options/command_line.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_separator_hh
#define options_separator_hh
#include <iostream>
#include <vector>
#include "options/base.hh"
#include "options/utils.hh"

namespace options
{
  //------------------------------------------------------------------
  /** Introduces a separator in option list */
  struct separator : base_option
  {
    /** 
     * Constructor 
     * 
     * @param title Title of separator 
     */
    separator(const std::string& title)
      : base_option('\0',"","",title)
    {}
    /** NO-OP */
    bool parse(const std::string& ="") { return true; }
    /** NO-OP */
    std::string to_string() const { return std::string(); }
    /** NO-OP */
    std::string title() const { return std::string(); }
    /** Show header in usage listing */
    void usage(std::ostream& o,size_t,size_t,size_t,size_t mcol=75) const
    {
      o << options::utils::wrap(description(),mcol) << ":\n";
    }
    /** Show header in usage listing */
    void show(std::ostream& o,size_t) const
    {
      o << "=== " << description() << " ===\n";
    }
  };
}
#endif
