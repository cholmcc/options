/**
 * @file      option.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_option_hh
#define options_option_hh
#include "options/base.hh"
#include "options/option_trait.hh"

namespace options
{
  //==================================================================
  /** 
   * Base template for options 
   */
  template <typename T, typename Trait=option_trait<T>>
  struct option : public base_option
  {
    using trait_type=Trait;
    using base=base_option;
    using string_initializer_list=typename base::string_initializer_list;
    using char_initializer_list=typename base::char_initializer_list;
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(const char_initializer_list&   shrt,
	   const string_initializer_list& lng,
	   const std::string&             arg,
	   const std::string&             desc,
	   const T&                       def=T())
      : base_option(shrt,lng,arg,desc), _value(def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(char               shrt,
	   const std::string& lng,
	   const std::string& arg,
	   const std::string& desc,
	   const T&           def=T())
      : option({shrt},{lng},arg,desc,def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(char                           shrt,
	   const string_initializer_list& lng,
	   const std::string&             arg,
	   const std::string&             desc,
	   const T&                       def=T())
      : option({shrt},lng,arg,desc,def)
    {}
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     * @param def  Default value
     */
    option(const char_initializer_list& shrt,
	   const std::string&           lng,
	   const std::string&           arg,
	   const std::string&           desc,
	   const T&                     def=T())
      : option(shrt,{lng},arg,desc,def)
    {}
    /** 
     * Parse an argument 
     *
     * @param arg Argument 
     *
     * @return true on success 
     */
    virtual bool parse(const std::string& arg="")
    {
      trait_type::from_string(_value, arg);
      return true;
    }
    /** @return Value as given type */
    operator T() const { return _value; }
    /** @return Value as given type */
    const T& value() const { return _value; }
    /** @return String representation of value */
    std::string to_string() const { return trait_type::to_string(_value); }
    /** Access */
    const T* operator->() const { return &_value; }
  protected:
    /** The value */
    T _value;
  };
}
#endif
