/**
 * @file      options/base.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A Command line options
 */
#ifndef options_base_hh
#define options_base_hh
#include <iostream>
#include <iomanip>
#include <vector>
#include <options/utils.hh>

namespace options
{
  //==================================================================
  /** 
   * Base class for options 
   */
  struct base_option
  {
    using char_initializer_list=std::initializer_list<char>;
    using string_initializer_list=std::initializer_list<std::string>;
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     */
    base_option(const char_initializer_list&   shrt,
		const string_initializer_list& lng,
		const std::string&             arg,
		const std::string&             desc)
      : _short(*shrt.begin()),
	_long(*lng.begin()),
	_arg(arg),
	_desc(desc),
	_short_alias(std::next(shrt.begin()),shrt.end()),
	_long_alias(std::next(lng.begin()),lng.end())
    {
      _register();
    }
    /** 
     * @param shrt Short option 
     * @param lng  Long option 
     * @param arg  Argument dummy 
     * @param desc Description 
     */
    base_option(char               shrt,
		const std::string& lng,
		const std::string& arg,
		const std::string& desc)
      : base_option({shrt},{lng},arg,desc)
    {}
    
    /** @return Description string */
    const std::string& description() const { return _desc; }
    /** @return Argument string */
    const std::string& argument() const { return _arg; }
    /** @return True if option need argument */
    bool has_argument() const { return _arg.size() > 0; }
    /** @return Short option */
    char shrt() const { return _short; }
    /** @return Long option */
    const std::string& lng() const { return _long; }
    /** 
     * Parse an argument 
     *
     * @param arg Argument 
     *
     * @return true on success 
     */
    virtual bool parse(const std::string& arg="") = 0;
    /** @return String representation of value */
    virtual std::string to_string() const = 0;

    virtual void usage(std::ostream& o,
		       size_t mlng,
		       size_t marg,
		       size_t mval,
		       size_t mcol=75) const
    {
      o << std::left;
      // First show aliases 
      size_t mx = std::max(_short_alias.size(),_long_alias.size());
      for (size_t i = 0; i < mx; i++) {
	char        s = (i < _short_alias.size() ? _short_alias[i] : '\0');
	std::string l = (i < _long_alias.size()  ? _long_alias[i]  : "");

	if (!s or s == ' ') o << "     ";
	else                o << "  -" << s << ",";
	if (!l.empty())     o << "--" << std::setw(mlng) << l+",";
	o << std::endl;
      }

      // Then show main options
      if (!_short or _short == ' ') o << "     ";
      else                          o << "  -" << shrt() << ",";
      if (_long.empty()) o << std::string(mlng+4,' ');
      else               o << "--" << std::setw(mlng) << _long << "  ";
      std::string arg = (has_argument() ? argument() : "");
      o << std::setw(marg) << arg << " [="
	<< std::setw(mval) << to_string() << "]  ";

      // Indention 
      size_t      ind = 3+1+1+2+mlng+2+marg+3+mval+3;
      std::string i   = "\n"+std::string(ind,' ');

      // Wrap description
      std::string d   = description();
      if (d.find(" - one of") == std::string::npos)
	d = utils::wrap(d,mcol-ind);

      // Do line-breaks with indention 
      size_t      p   = 0;
      while ((p = d.find("\n", p)) != std::string::npos) {
	d.replace(p, 1, i);
	p += i.length();
      }
      o << std::right << d << "\n";

    }
    /** Title (shortend description) */
    virtual std::string title(size_t mlen=40) const
    {
      std::string s = description().substr(0,description().find(" - one of"));
      if (s.length() > mlen) {
	auto p = s.rfind(' ',mlen);
	if (p == std::string::npos) s = s.substr(0,37)+"...";
	else                        s = s.substr(0,p)+" ...";
      }
      return s;
    }
    /** Show this option */
    virtual void show(std::ostream& o, size_t mtit) const
    {
      std::string t = title();
      o << t << ":" << std::string(mtit-t.size(),' ') << " "
	<< to_string() << "\n";
    }
  protected:
    /** Register self with handler */
    virtual void _register(); // Not implemented here
    /** Short option */
    char        _short;
    /** Long option */
    std::string _long;
    /** Dummy argument */
    std::string _arg;
    /** Description */
    std::string _desc;
    /** Short aliases */
    std::vector<char> _short_alias;
    /** Long aliases */
    std::vector<std::string> _long_alias;
  };
}
#endif
